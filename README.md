<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# divert-and-modify - use dpkg-divert to stash and modify files

## Overview

The `divert-and-modify` library uses `dpkg-divert` to replace files owned by
other packages and modify them in predetermined ways.
This allows software packages to "patch" other tools and libraries.

## Configuration file format

The only format that the `divert-and-modify` library understands so far is
a series of TOML files in the following layout:

```toml
[format.version]
major = 0
minor = 1

[[divmod.changes]]
name = "fix-something"
path = "/path/to/file/to/change"
handler.mode = "regex-sub"
handler.regex_sub.pattern = "regular expression to match"
handler.regex_sub.subst = "replacement string possibly including backreferences"
# Optional, defaults to false
same-ok = true
# Optional, defaults to an empty list
allowed-suffixes = [".fixed", ".patched"]
```

## Supported modification handlers

### regex-sub - substitute regular expressions within single lines

The `regex-sub` handler examines the file line by line and applies a regular
expression substitution to each line in order.
The regular expression is in the format understood by the Python `re` module.
The substitution may contain backreferences, both in the `\1` and
`\g<name>` format.

## To do

Break the configuration file search out into a separate overdrop-style library.
