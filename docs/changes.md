<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the divert-and-modify project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2023/12/01

### Fixes

- Make the `sync` subcommand remove existing file diversions that are no longer defined in
  the configuration files, e.g. ones that were defined by a Debian package that has since
  been removed

### Additions

- Add the `undo --all` subcommand that removes all diversions, even if they are still
  described in the configuration files

## [0.1.2] - 2023/11/29

### Additions

- Also load the `same-ok` and `allowed-suffixes` change attributes from the configuration
- Test suite:
    - test all the attributes of changes loaded from the configuration files

## [0.1.1] - 2023/11/28

### Fixes

- Make sure the updated file has the same permission mode as the diverted one
- Remove the updated file when the diverted one is gone (e.g. the Debian package
  that provided the original file was removed)
- Build infrastructure:
    - lower the versions of the `click` and `tomli` requirements to match those in
      Debian 11 (bullseye)
- Test suite:
    - `test_docker`:
        - check the output of `apt-cache policy` to figure out which dependency
          packages need to be installed from the regular repositories and which ones
          should be installed from the backports suite

### Additions

- Test suite:
    - `test_docker`:
        - allow arguments to be passed to the `test_docker` tool in the "docker" Tox environment
        - test the lifecycle of a file that already exists when `divert-and-modify sync` is
          run for the first time

### Other changes

- Test suite:
    - `test_docker`:
        - install the dependencies as Debian packages, not Python ones in the virtual environment
        - move the Python source to a `tests/docker/python/` subtree
        - rewrite the tool invoked within the Docker test container in Python

## [0.1.0] - 2023/11/24

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/divert-and-modify/-/compare/release%2F0.1.3...main
[0.1.3]: https://gitlab.com/ppentchev/divert-and-modify/-/compare/release%2F0.1.2...release%2F0.1.3
[0.1.2]: https://gitlab.com/ppentchev/divert-and-modify/-/compare/release%2F0.1.1...release%2F0.1.2
[0.1.1]: https://gitlab.com/ppentchev/divert-and-modify/-/compare/release%2F0.1.0...release%2F0.1.1
[0.1.0]: https://gitlab.com/ppentchev/divert-and-modify/-/tags/release%2F0.1.0
