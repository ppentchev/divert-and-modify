# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Command-line tool for applying file diversions and updates."""

from __future__ import annotations

import sys
import typing

import click

from divert_and_modify import apply
from divert_and_modify import config
from divert_and_modify import defs
from divert_and_modify import util


if typing.TYPE_CHECKING:
    from typing import Final


@click.command(name="list")
def _cmd_list() -> None:
    """Load the configuration, list the predefined changes."""
    cfg: Final = defs.Config(
        log=util.build_logger(verbose=False), utf8_env=util.get_deb_utf8_environment()
    )
    changes: Final = config.load_changes(cfg)
    for change in changes:
        print(f"{change.path}\t{change.name}")


@click.command(name="sync")
@click.option("--verbose", "-v", is_flag=True)
def _cmd_sync(*, verbose: bool) -> None:
    """Read the change definitions, examine the dpkg diversions, make any changes necessary."""
    cfg: Final = defs.Config(
        log=util.build_logger(verbose=verbose), utf8_env=util.get_deb_utf8_environment()
    )
    changes: Final = config.load_changes(cfg)
    apply.apply_changes(cfg, changes)


@click.command(name="undo")
@click.option("--all", is_flag=True)
@click.option("--verbose", "-v", is_flag=True)
def _cmd_undo(*, all: bool, verbose: bool) -> None:  # noqa: A002
    """Invoke the "clean up the orphans" function to remove all diversions."""
    if not all:
        sys.exit("No mode (e.g. `--all`) specified for `undo`")

    cfg: Final = defs.Config(
        log=util.build_logger(verbose=verbose), utf8_env=util.get_deb_utf8_environment()
    )
    apply.clean_up_orphans(cfg, set())


@click.group(name="divert-and-modify")
def main() -> None:
    """Expect a subcommand."""


main.add_command(_cmd_list)
main.add_command(_cmd_sync)
main.add_command(_cmd_undo)
