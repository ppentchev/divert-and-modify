# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Parse the TOML configuration files."""

from __future__ import annotations

import dataclasses
import errno
import re
import sys
import typing

import typedload

from divert_and_modify import config_v0
from divert_and_modify import defs


if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib


if typing.TYPE_CHECKING:
    import pathlib
    from typing import Final


@dataclasses.dataclass
class LoadError(Exception):
    """An error occurred while loading data from a config file."""

    path: pathlib.Path
    """The file we tried to load."""

    err: Exception
    """The error that occurred."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Could not load the {self.path} configuration file: {self.err}"


def convert_regex_sub(
    dummy_change: defs.DivertChange, handler: config_v0.RegexSubHandler
) -> defs.DivertChange:
    """Return a handler that applies a substitution to all the lines in a file."""
    rex: Final = re.compile(handler.pattern)

    def modify(_cfg: defs.Config, _dfile: defs.DivertChange, contents: str) -> str:
        """Apply a substitution to all the lines in a file."""
        lines: Final = contents.splitlines()
        return "".join(rex.sub(handler.subst, line) + "\n" for line in lines)

    return dataclasses.replace(dummy_change, modify=modify)


def convert_change(change: config_v0.DivertChange) -> defs.DivertChange:
    """Parse a loaded configuration change description."""
    if not change.path.is_absolute():
        raise ValueError((change, change.path))
    handler: Final = change.handler

    def dummy_handler(cfg: defs.Config, change: defs.DivertChange, contents: str) -> str:
        """Complain about being invoked."""
        raise NotImplementedError(repr((cfg, change, contents)))

    dummy_change: Final = defs.DivertChange(
        name=change.name,
        path=change.path.resolve(),
        modify=dummy_handler,
        same_ok=change.same_ok,
        allowed_suffixes=change.allowed_suffixes,
    )

    if handler.mode == config_v0.Mode.REGEX_SUB:
        if handler.regex_sub is None:
            raise ValueError(repr(change))
        converted = convert_regex_sub(dummy_change, handler.regex_sub)
        if converted.modify is dummy_handler:
            raise RuntimeError(repr((change, dummy_change)))
        return converted

    raise NotImplementedError(repr(handler.mode))


def load_changes_file(cfg: defs.Config, path: pathlib.Path) -> list[defs.DivertChange]:
    """Load change definitions from the specified configuration file."""
    cfg.log.debug("- loading change definitions from %(path)s", {"path": path})
    try:
        contents: Final = path.read_text(encoding="UTF-8")
        raw: Final = tomllib.loads(contents)

        ver_major: Final = raw["format"]["version"]["major"]
        if not isinstance(ver_major, int):
            raise TypeError(("format.version.major", ver_major))  # noqa: TRY301

        ver_minor: Final = raw["format"]["version"]["major"]
        if not isinstance(ver_minor, int):
            raise TypeError(("format.version.minor", ver_minor))  # noqa: TRY301

        if ver_major != 0:
            raise RuntimeError(("ver_major", 0))

        return [
            convert_change(change)
            for change in typedload.load(raw, config_v0.ConfigTop).divmod.changes
        ]
    except (OSError, ValueError, TypeError, KeyError) as err:
        raise LoadError(path, err) from err


def load_changes(
    cfg: defs.Config, *, dirs: list[pathlib.Path] | None = None
) -> list[defs.DivertChange]:
    """Find configuration files and parse them."""
    if dirs is None:
        dirs = [defs.DEFAULT_SHARE_PATH, defs.DEFAULT_ETC_PATH]

    # Walk the config dirs in order, let files from later directories
    # override files with the same name from earlier ones.
    #
    # This should be done by a separate library, overdrop-style.
    res: Final = []
    cfg_files: Final[dict[str, pathlib.Path]] = {}
    for cfg_dir in dirs:
        if not cfg_dir.exists():
            continue
        if not cfg_dir.is_dir():
            raise NotADirectoryError(errno.ENOTDIR, cfg_dir)
        cfg.log.debug(
            "Looking for divert-and-modify config files in %(cfg_dir)s", {"cfg_dir": cfg_dir}
        )

        for path in cfg_dir.iterdir():
            # Let a symlink to `/dev/null` in a later directory make us forget about
            # any files by that name in earlier ones.
            if path.is_symlink() and str(path.readlink()) == "/dev/null":
                if path.name in cfg_files:
                    del cfg_files[path.name]
                continue

            if path.is_file() and not path.name.startswith(".") and path.name.endswith(".toml"):
                cfg_files[path.name] = path

    # Now load the config files in alphabetical order of their names
    for _, path in sorted(cfg_files.items()):
        res.extend(load_changes_file(cfg, path))

    return res
