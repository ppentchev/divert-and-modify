# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Divert a file, update its contents."""

from __future__ import annotations

import collections
import dataclasses
import functools
import itertools
import pathlib
import re
import subprocess
import tempfile
import typing

from divert_and_modify import defs


if typing.TYPE_CHECKING:
    from typing import Final


RE_DIVERTED: Final = re.compile(
    r""" ^
       diversion \s of \s
       (?P<source> .+ )
       \s to \s
       (?P<target> .+ )
       \s by \s
       (?P<package> \S+ )
       $ """,
    re.X,
)


@dataclasses.dataclass
class ExamineDivertError(Exception):
    """We could not query `dpkg-divert` for diversions for a specific path."""

    path: pathlib.Path
    """The path we tried to query for."""

    err: Exception
    """The error that occurred."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Could not query `dpkg-divert` for {self.path}: {self.err}"


@dataclasses.dataclass
class TooManyDiversionsError(Exception):
    """The `dpkg-divert --list` output consisted of more than one line."""

    path: pathlib.Path
    """The path we tried to query for."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Too many diversions reported by `dpkg-divert` for {self.path}"


@dataclasses.dataclass
class UnexpectedDivertOutputError(Exception):
    """We could not parse the `dpkg-divert --list` output for a specific path."""

    path: pathlib.Path
    """The path we tried to query for."""

    line: str
    """The line we could not parse."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Unexpected `dpkg-divert --list` output for {self.path}: {self.line!r}"


@dataclasses.dataclass
class DivertedElsewhereError(Exception):
    """The file is diverted to an unexpected location, probably not by us."""

    path: pathlib.Path
    """The path we tried to query for."""

    target: pathlib.Path
    """The location that we tried to divert the file to."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Did not expect {self.path} to be diverted to {self.target}"


@dataclasses.dataclass
class UnsupportedDivModVersionError(Exception):
    """The file is diverted to an unexpected location, probably not by us."""

    path: pathlib.Path
    """The path we tried to query for."""

    dm_ver: str
    """The `divert-and-modify` version string that we encountered."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Unexpected divert-and-modify version in the {self.path} diversion: {self.dm_ver}"


@dataclasses.dataclass
class CouldNotDivertError(Exception):
    """Something went wrong with `dpkg-divert --add`."""

    path: pathlib.Path
    """The path we tried to query for."""

    target: pathlib.Path
    """The unexpected location that the file is diverted to."""

    err: Exception
    """The error that occurred."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Could not divert {self.path} to {self.target}: {self.err}"


@dataclasses.dataclass
class CouldNotUndivertError(Exception):
    """Something went wrong with `dpkg-divert --remove`."""

    path: pathlib.Path
    """The path we tried to query for."""

    target: pathlib.Path
    """The unexpected location that the file is diverted to."""

    err: Exception
    """The error that occurred."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"Could not remove the diversion of {self.path} to {self.target}: {self.err}"


@dataclasses.dataclass
class NoChangeError(Exception):
    """We tried to apply a change, but it did not modify the file's contents."""

    path: pathlib.Path
    """The file we tried to update."""

    name: str
    """The name of the change that did not do anything."""

    def __str__(self) -> str:
        """Provide a human-readable description of the error."""
        return f"The '{self.name}' change applied to {self.path} did not modify its contents"


def is_already_diverted(
    cfg: defs.Config, path: pathlib.Path, allowed_suffixes: list[str]
) -> pathlib.Path | None:
    """Check whether we already diverted a file.

    Raise different exceptions in case of different errors.
    """
    cfg.log.debug("Checking whether %(path)s is already diverted", {"path": path})
    try:
        list_lines: Final = subprocess.check_output(
            ["dpkg-divert", "--list", "--", path], encoding="UTF-8", env=cfg.utf8_env
        ).splitlines()
    except (OSError, subprocess.CalledProcessError) as err:
        cfg.log.debug("- `dpkg-divert --list` failed: %(err)s", {"err": err})
        raise ExamineDivertError(path, err) from err
    cfg.log.debug(
        "- examining %(count)d line(s) of `dpkg-divert --list` output", {"count": len(list_lines)}
    )
    # The following checks will look better with structural pattern matching
    if not list_lines:
        cfg.log.debug("- not diverted")
        return None
    if len(list_lines) != 1:
        raise TooManyDiversionsError(path)

    line: Final = list_lines[0]
    cfg.log.debug("- examining a single diversion line: %(line)r", {"line": line})
    div: Final = RE_DIVERTED.match(line)
    if div is None:
        raise UnexpectedDivertOutputError(path, line)

    target: Final = pathlib.Path(div.group("target"))
    cfg.log.debug(
        "- apparently %(path)s is diverted to %(target)s, do we like that?",
        {"path": path, "target": target},
    )
    if target.parent != path.parent:
        raise DivertedElsewhereError(path, target)
    rest: Final = target.name.removeprefix(path.name)
    if rest == target.name:
        raise DivertedElsewhereError(path, target)

    if rest in allowed_suffixes:
        cfg.log.debug(
            "- somebody else diverted %(path)s using a different suffix: %(rest)s",
            {"path": path, "rest": rest},
        )
        return target
    dm_ver: Final = rest.removeprefix(".divmod.")
    if dm_ver == rest:
        raise DivertedElsewhereError(path, target)

    if dm_ver != "v0":
        raise UnsupportedDivModVersionError(path, dm_ver)

    cfg.log.debug("- it seems we have already been here")
    return target


def get_diverted_path(path: pathlib.Path) -> pathlib.Path:
    """Build the path to divert a file to."""
    return path.with_name(path.name + ".divmod.v0")


def ensure_diverted(
    cfg: defs.Config, path: pathlib.Path, *, allowed_suffixes: list[str]
) -> pathlib.Path:
    """Make sure the specified file is diverted as we expect it to be."""
    current_path: Final = is_already_diverted(cfg, path, allowed_suffixes=allowed_suffixes)
    if current_path is not None:
        return current_path

    diverted_path: Final = get_diverted_path(path)
    try:
        subprocess.check_call(
            [
                "dpkg-divert",
                "--rename",
                "--divert",
                diverted_path,
                "--package",
                defs.DIVERT_PACKAGE_NAME,
                "--add",
                path,
            ],
            env=cfg.utf8_env,
        )
    except (OSError, subprocess.CalledProcessError) as err:
        cfg.log.error(  # noqa: TRY400
            "Could not divert %(path)s to %(diverted_path)s: %(err)s",
            {"path": path, "diverted_path": diverted_path, "err": err},
        )
        raise CouldNotDivertError(path, diverted_path, err) from err

    return diverted_path


def apply_content_changes(
    cfg: defs.Config, path: pathlib.Path, changes: list[defs.DivertChange]
) -> str:
    """Read a file's contents, apply the requested changes."""

    def single(contents: str, change: defs.DivertChange) -> str:
        """Apply a single change, make sure something changed."""
        updated: Final = change.modify(cfg, change, contents)
        if (not change.same_ok) and updated == contents:
            cfg.log.error(
                "The '%(name)s' change to %(path)s did not change the contents",
                {"name": change.name, "path": path},
            )
            raise NoChangeError(path, change.name)
        return updated

    current: Final = path.read_text(encoding="UTF-8")
    return functools.reduce(single, changes, current)


def apply_to_file(cfg: defs.Config, path: pathlib.Path, changes: list[defs.DivertChange]) -> None:
    """Divert and modify a single file."""
    if any(change.path != path for change in changes):
        raise RuntimeError(repr((cfg, changes)))
    allowed_suffixes: Final = sorted(
        set(itertools.chain(*(change.allowed_suffixes for change in changes)))
    )

    # First, check whether the file is already diverted
    diverted_path: Final = ensure_diverted(cfg, path, allowed_suffixes=allowed_suffixes)

    # Now, if necessary, create or update a file with the original name
    if diverted_path.exists():
        cfg.log.debug(
            "- checking whether we need to convert %(diverted_path)s to %(path)s",
            {"path": path, "diverted_path": diverted_path},
        )
        contents: Final = apply_content_changes(cfg, diverted_path, changes)
        try:
            same = (path.read_text(encoding="UTF-8") == contents) and (
                (path.stat().st_mode & 0o7777) == (diverted_path.stat().st_mode & 0o7777)
            )
            cfg.log.debug("- a %(path)s file exists, same: %(same)s", {"path": path, "same": same})
        except FileNotFoundError:
            same = False
            cfg.log.debug("- a %(path)s file does not even exist", {"path": path})
        if not same:
            cfg.log.debug("- creating or updating %(path)s", {"path": path})
            with tempfile.NamedTemporaryFile(
                dir=path.parent,
                prefix=path.name + ".divmod.temp.",
                mode="wt",
                encoding="UTF-8",
            ) as tempf_obj:
                print(contents, file=tempf_obj, end="", flush=True)

                tempf: Final = pathlib.Path(tempf_obj.name)
                tempf.chmod(diverted_path.stat().st_mode & 0o7777)
                tempf.rename(path)

                tempf_obj.delete = False
                # Sigh...
                if hasattr(tempf_obj, "_closer"):
                    tempf_obj._closer.delete = False  # noqa: SLF001
    elif path.is_file():
        cfg.log.debug("- removing %(path)s", {"path": path})
        path.unlink()


def apply_changes(cfg: defs.Config, changes: list[defs.DivertChange]) -> None:
    """Examine the filesystem, divert files, modify them."""
    by_path: Final[collections.defaultdict[str, list[defs.DivertChange]]] = collections.defaultdict(
        list
    )
    for change in changes:
        by_path[str(change.path)].append(change)
    cfg.log.debug("About to apply changes to %(count)d files", {"count": len(by_path)})
    for path_changes in by_path.values():
        apply_to_file(cfg, path_changes[0].path, path_changes)

    clean_up_orphans(cfg, set(by_path.keys()))


def clean_up_orphans(cfg: defs.Config, known: set[str]) -> None:
    """Remove unknown or obsolete file diversions."""
    lines: Final = subprocess.check_output(
        ["dpkg-divert", "--list"], encoding="UTF-8", env=cfg.utf8_env
    ).splitlines()
    for line in lines:
        mdiv = RE_DIVERTED.match(line)
        if mdiv is None:
            continue
        source, target, package = mdiv.group("source"), mdiv.group("target"), mdiv.group("package")
        if package != defs.DIVERT_PACKAGE_NAME or source in known:
            continue

        cfg.log.debug(
            "Removing a diversion from %(source)s to %(target)s",
            {"source": source, "target": target},
        )
        path_source = pathlib.Path(source)
        try:
            if path_source.is_file() or path_source.is_symlink():
                path_source.unlink()
            subprocess.check_call(
                [
                    "dpkg-divert",
                    "--rename",
                    "--divert",
                    target,
                    "--package",
                    package,
                    "--remove",
                    source,
                ],
                env=cfg.utf8_env,
            )
        except (subprocess.CalledProcessError, OSError) as err:
            raise CouldNotUndivertError(path_source, pathlib.Path(target), err) from err
