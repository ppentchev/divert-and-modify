# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the divert-and-modify package."""

from __future__ import annotations

import dataclasses
import pathlib
import typing
from collections.abc import Callable


if typing.TYPE_CHECKING:
    import logging
    from typing import Final


VERSION: Final = "0.1.3"
"""The divert-and-modify library version, SemVer-like."""

DIVERT_PACKAGE_NAME: Final = "divert-and-modify"
"""The package name that will be passed to `dpkg-divert`."""

DEFAULT_SHARE_PATH: Final = pathlib.Path("/usr/share/divert-and-modify/changes.d")
DEFAULT_ETC_PATH: Final = pathlib.Path("/etc/divert-and-modify/changes.d")


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the divert-and-modify library."""

    log: logging.Logger
    """Output diagnostic and informational messages."""

    utf8_env: dict[str, str]
    """Environment settings to pass to child processes to ensure valid UTF-8 output."""


@dataclasses.dataclass(frozen=True)
class DivertChange:
    """Describe a single file to be diverted."""

    name: str
    """A human-readable identifier for this change."""

    path: pathlib.Path
    """The file to be diverted."""

    modify: ModifyHandler
    """The callback function that modifies the file's contents."""

    same_ok: bool = False
    """Allow the handler to not modify the file contents at all."""

    allowed_suffixes: list[str] = dataclasses.field(default_factory=list)
    """Allowed suffixes (except the ".divmod.v0" one) for files diverted by other programs."""


ModifyHandler = Callable[[Config, DivertChange, str], str]
"""A callback function that modifies the contents of a diverted file.

The function accepts a single argument, the contents of the file.
It must return the updated contents of the file.
"""
