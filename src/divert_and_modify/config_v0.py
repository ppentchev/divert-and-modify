# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Define the version 0 configuration file structure."""

import dataclasses
import enum
import pathlib
from typing import List, Optional


class Mode(str, enum.Enum):
    """The way to modify the file."""

    REGEX_SUB = "regex-sub"


@dataclasses.dataclass(frozen=True)
class RegexSubHandler:
    """Replace one or more lines in a file."""

    pattern: str
    """The regular expression pattern to look for."""

    subst: str
    """The string (possibly including backreferences) to replace with."""


@dataclasses.dataclass(frozen=True)
class ChangeHandler:
    """The defintion of the way to modify the file."""

    mode: Mode
    """The way to modify the file."""

    regex_sub: Optional[RegexSubHandler]
    """The definition of the "regex-sub" handler."""


@dataclasses.dataclass(frozen=True)
class DivertChange:
    """A single change to be applied."""

    name: str
    """The human-readable identifier for this change."""

    path: pathlib.Path
    """The path to the file to be diverted and updated."""

    handler: ChangeHandler
    """The definition of the way to modify the file."""

    same_ok: bool = dataclasses.field(default=False, metadata={"name": "same-ok"})
    """Allow the handler to not modify the file contents at all."""

    allowed_suffixes: List[str] = dataclasses.field(
        default_factory=list, metadata={"name": "allowed-suffixes"}
    )
    """Allowed suffixes (except the ".divmod.v0" one) for files diverted by other programs."""


@dataclasses.dataclass(frozen=True)
class ConfigDivMod:
    """The divert-and-modify configuration section."""

    changes: List[DivertChange]
    """The list of changes."""


@dataclasses.dataclass(frozen=True)
class ConfigTop:
    """The top-level configuration file format."""

    divmod: ConfigDivMod  # noqa: A003  # there can be no confusion
    """The divert-and-modify configuration section."""
