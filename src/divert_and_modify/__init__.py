# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Use dpkg-divert to apply changes to a file from another package."""

from __future__ import annotations


# isort: off

from .apply import apply_changes
from .defs import Config, DivertChange
from .util import get_deb_utf8_environment

# isort: on


__all__ = ["Config", "DivertChange", "apply_changes", "get_deb_utf8_environment"]
