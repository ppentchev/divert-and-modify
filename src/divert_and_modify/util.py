# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the divert-and-modify package."""

from __future__ import annotations

import functools
import logging
import os
import sys
import typing


if typing.TYPE_CHECKING:
    from typing import Final, TextIO


# Shamelessly stolen from logging-std
@functools.lru_cache
def build_logger(
    *, verbose: bool = False, error_stream: TextIO | None = None, info_stream: TextIO | None = None
) -> logging.Logger:
    """Build a logger that outputs to the standard output and error streams.

    Messages of level `INFO` go to the standard output stream.
    Messages of level `WARNING` and higher go to the standard error stream.
    If `verbose` is true, messages of level `DEBUG` also go to the standard error stream.

    The stream to send error messages to may be overridden by the `error_stream` parameter,
    and the one for informational messages - by the `info_stream` one.
    """
    logger: Final = logging.getLogger("logging_std")
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.propagate = False

    if error_stream is None:
        error_stream = sys.stderr
    diag_handler: Final = logging.StreamHandler(error_stream)
    diag_handler.setLevel(logging.DEBUG if verbose else logging.WARNING)
    diag_handler.addFilter(lambda rec: rec.levelno != logging.INFO)
    logger.addHandler(diag_handler)

    if info_stream is None:
        info_stream = sys.stdout
    info_handler: Final = logging.StreamHandler(info_stream)
    info_handler.setLevel(logging.INFO)
    info_handler.addFilter(lambda rec: rec.levelno == logging.INFO)
    logger.addHandler(info_handler)

    return logger


def get_deb_utf8_environment(*, env: dict[str, str] | None = None) -> dict[str, str]:
    """Set the `LC_ALL` and `LANGUAGE` variables to values that work under Debian GNU/Linux."""
    env = dict(os.environ) if env is None else dict(env)

    env["LC_ALL"] = "C.UTF-8"
    if "LANGUAGE" in env:
        del env["LANGUAGE"]
    return env
