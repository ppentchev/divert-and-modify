# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

click >= 7, < 9
tomli >= 1, < 3; python_version < "3.11"
typedload >= 2.8, < 3
