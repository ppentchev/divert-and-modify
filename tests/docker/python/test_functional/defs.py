# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the functional test for `divert-and-modify`."""

from __future__ import annotations

import dataclasses
import typing


if typing.TYPE_CHECKING:
    import logging
    import pathlib


@dataclasses.dataclass(frozen=True)
class State:
    """The current runtime state of the tests."""

    log: logging.Logger
    """Somewhere to send informational, error, and diagnostic messages to."""

    utf8_env: dict[str, str]
    """Environment settings suitable for running processes with UTF-8 output."""

    codename: str
    """The codename of the Debian release."""

    backports_enabled: bool
    """True if we already enabled the backports repository."""

    apt_db_updated: bool
    """True if we already ran `apt-get update`."""

    divmod_bin: pathlib.Path | None
    """The path to the divert-and-modify executable to test."""
