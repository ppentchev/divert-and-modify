# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Invoke apt and dpkg, update the current state."""

from __future__ import annotations

import dataclasses
import pathlib
import re
import subprocess
import sys
import typing


if typing.TYPE_CHECKING:
    from typing import Final

    from . import defs


RE_POLICY_CANDIDATE: Final = re.compile(
    r""" ^
	\s* Candidate \s* : \s*
	[0-9]
	""",
    re.X,
)
"""Match a true installation candidate line in the `apt-cache policy` output."""


def dpkg_list_installed(state: defs.State) -> dict[str, str]:
    """Get the currently installed packages and their versions."""
    lines: Final = subprocess.check_output(
        ["dpkg-query", "-W", "-f", r"${Package}\t${Version}\t${db:Status-Abbrev}\n"],
        encoding="UTF-8",
        env=state.utf8_env,
    ).splitlines()

    res: Final[dict[str, str]] = {}
    for line in lines:
        fields = line.strip().split("\t")
        try:
            name, version, db_state = fields
        except ValueError:
            sys.exit(f"Unexpected output from dpkg-query: {line!r}")
        if db_state != "ii":
            continue
        if name in res:
            sys.exit(
                f"Duplicate {name!r} in the dpkg-query output: had {res[name]!r}, now {version!r}"
            )
        res[name] = version

    return res


def apt_update_if_needed(state: defs.State) -> defs.State:
    """Run `apt-get update` once."""
    if state.apt_db_updated:
        return state

    state.log.info("Running `apt-get update`")
    subprocess.check_call(["apt-get", "update"], env=state.utf8_env)
    return dataclasses.replace(state, apt_db_updated=True)


def apt_install(state: defs.State, names: list[str], *, from_backports: bool = False) -> defs.State:
    """Install one or more packages."""
    if not names:
        raise RuntimeError(repr((state, names)))
    state = apt_update_if_needed(state)

    state.log.info(
        "Running `apt-get install` %(backp)sfor %(names)s",
        {"backp": "from the backports suite " if from_backports else "", "names": " ".join(names)},
    )
    args_backports: Final = ["-t", f"{state.codename}-backports"] if from_backports else []
    subprocess.check_call(
        [
            "env",
            "DEBIAN_FRONTEND=noninteractive",
            "apt-get",
            "-y",
            "install",
            *args_backports,
            "--",
            *names,
        ],
        env=state.utf8_env,
    )
    return state


def apt_split_by_candidate(
    state: defs.State, names: list[str]
) -> tuple[defs.State, list[str], list[str]]:
    """Run `apt-cache policy` for the specified packages, see what it says."""
    from_std: Final = []
    from_backports: Final = []
    for name in names:
        state.log.info("Querying `apt-cache policy` for %(name)s", {"name": name})
        lines = subprocess.check_output(
            ["apt-cache", "policy", "--", name], encoding="UTF-8", env=state.utf8_env
        ).splitlines()
        if any(RE_POLICY_CANDIDATE.match(line) for line in lines):
            state.log.info("- found it in the standard repositories")
            from_std.append(name)
        else:
            state.log.info("- apparently we need to install it from the backports suite")
            from_backports.append(name)

    return state, from_std, from_backports


def enable_backports(state: defs.State) -> defs.State:
    """Run `apt-cache policy` for the specified packages, see what it says."""
    if state.backports_enabled:
        return state

    if state.codename in ("sid", "unstable"):
        sys.exit("No backports for Debian unstable")

    pathlib.Path("/etc/apt/sources.list.d/divmod-test-backports.list").write_text(
        f"deb http://deb.debian.org/debian {state.codename}-backports main\n", encoding="UTF-8"
    )
    pathlib.Path("/etc/apt/preferences.d/divmod-test-backports.pref").write_text(
        f"Package: *\nPin: release a={state.codename}-backports\nPin-Priority: -1\n",
        encoding="UTF-8",
    )

    return apt_update_if_needed(dataclasses.replace(state, apt_db_updated=False))


def apt_install_if_needed(
    state: defs.State, names: list[str], *, allow_backports: bool = False
) -> defs.State:
    """Install some packages if they are not already installed."""
    installed: Final = dpkg_list_installed(state)
    missing: Final = [name for name in names if name not in installed]
    if not missing:
        state.log.info("Already installed: %(names)s", {"names": " ".join(names)})
        return state

    if allow_backports:
        state, from_std, from_backports = apt_split_by_candidate(state, missing)
        if from_std:
            state = apt_install(state, from_std)

        if from_backports:
            state = enable_backports(state)
            return apt_install(state, from_backports, from_backports=True)

    return apt_install(state, names)


def apt_remove(state: defs.State, names: list[str]) -> defs.State:
    """Remove one or more packages."""
    subprocess.check_call(
        ["env", "DEBIAN_FRONTEND=noninteractive", "apt-get", "-y", "remove", "--", *names],
        env=state.utf8_env,
    )
    return state
