# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Run a couple of `divert-and-modify` tests on a system we can break."""

from __future__ import annotations

import dataclasses
import logging
import os
import pathlib
import re
import shlex
import subprocess
import sys
import typing

from . import defs
from . import pkg_apt
from . import steps


if typing.TYPE_CHECKING:
    from typing import Final


RE_PIP_LOCATION: Final = re.compile(
    r""" ^
    Location: \s*
    (?P<loc> \S+ )
    $ """,
    re.X,
)
"""Match a package's location line in the output of `pip show`."""

RE_PIP_BIN: Final = re.compile(
    r""" ^
    \s+
    (?P<relpath> \S+ /bin/divert-and-modify )
    $ """,
    re.X,
)

OPT_BREAK_SYS: Final = "--break-system-packages"
"""The option to pass to `pip install` to force installing a package."""


def get_deb_utf8_environment(*, env: dict[str, str] | None = None) -> dict[str, str]:
    """Set the `LC_ALL` and `LANGUAGE` variables to values that work under Debian GNU/Linux."""
    env = dict(os.environ) if env is None else dict(env)

    env["LC_ALL"] = "C.UTF-8"
    if "LANGUAGE" in env:
        del env["LANGUAGE"]
    return env


def look_around(utf8_env: dict[str, str]) -> defs.State:
    """Examine the initial state of the system we are running on."""
    logging.basicConfig()
    log: Final = logging.getLogger()
    log.setLevel(logging.DEBUG)

    log.info("Trying to figure out what Debian release we are running")
    codename_lines: Final = subprocess.check_output(
        [
            "sh",
            "-c",
            "unset VERSION_CODENAME; . /etc/os-release; printf -- '%s\\n' \"$VERSION_CODENAME\"",
        ],
        encoding="UTF-8",
        env=utf8_env,
    ).splitlines()
    if len(codename_lines) != 1 or not codename_lines[0]:
        sys.exit(
            f"Expected exactly one non-empty line for VERSION_CODENAME, got {codename_lines!r}"
        )
    codename: Final = codename_lines[0]
    log.info("Got codename %(codename)s", {"codename": codename})

    return defs.State(
        log=log,
        utf8_env=utf8_env,
        codename=codename,
        backports_enabled=False,
        apt_db_updated=False,
        divmod_bin=None,
    )


def find_divmod_wheel(state: defs.State) -> pathlib.Path:
    """Find the wheel file to install."""
    distdir: Final = pathlib.Path("/opt/dist")
    state.log.info("Looking for a wheel file in %(distdir)s", {"distdir": distdir})
    candidates: Final = [
        path
        for path in distdir.iterdir()
        if path.is_file() and path.name.startswith("divert") and path.name.endswith(".whl")
    ]
    if len(candidates) != 1:
        sys.exit(f"Expected to find exactly one /opt/dist/divert*.whl file, got {candidates!r}")
    res: Final = candidates[0]
    state.log.info("- found %(res)s", {"res": res})
    return res


def install_divmod(state: defs.State) -> defs.State:
    """Install divert-and-modify as a local package."""
    wheelfile: Final = find_divmod_wheel(state)
    opt_break_sys: Final = (
        [OPT_BREAK_SYS]
        if OPT_BREAK_SYS
        in subprocess.check_output(
            [sys.executable, "-m", "pip", "install", "--help"], encoding="UTF-8", env=state.utf8_env
        )
        else []
    )
    state.log.info(
        "Installing %(wheelfile)s, additional options: %(break)s",
        {"wheelfile": wheelfile, "break": shlex.join(opt_break_sys)},
    )
    subprocess.check_call(
        [
            sys.executable,
            "-m",
            "pip",
            "install",
            "--user",
            *opt_break_sys,
            "-i",
            "http://127.0.0.1:7/",
            "--",
            wheelfile,
        ],
        env=state.utf8_env,
    )
    return state


def find_divmod_bin(state: defs.State) -> defs.State:
    """Find the location of the installed divert-and-modify executable file."""
    state.log.info("Looking for the installed divert-and-modify executable file")
    lines: Final = subprocess.check_output(
        [sys.executable, "-m", "pip", "show", "-f", "--", "divert-and-modify"],
        encoding="UTF-8",
        env=state.utf8_env,
    ).splitlines()
    loc_lines: Final = [
        mloc.group("loc") for mloc in (RE_PIP_LOCATION.match(line) for line in lines) if mloc
    ]
    if len(loc_lines) != 1:
        sys.exit(f"Expected exactly one Location line in the `pip show` output, got {lines!r}")
    bin_lines: Final = [
        mbin.group("relpath") for mbin in (RE_PIP_BIN.match(line) for line in lines) if mbin
    ]
    if len(loc_lines) != 1:
        sys.exit(
            f"Expected exactly one */bin/divert-and-modify line in the `pip show` output, "
            f"got {lines!r}"
        )
    divmod_bin: Final = (pathlib.Path(loc_lines[0]) / bin_lines[0]).resolve()
    state.log.info("- found %(bin)s", {"bin": divmod_bin})
    if not divmod_bin.is_file():
        sys.exit(f"Expected {divmod_bin} to be a file")
    return dataclasses.replace(state, divmod_bin=divmod_bin)


def main() -> None:
    """Parse command-line options, run the tests."""
    utf8_env: Final = get_deb_utf8_environment()
    state = look_around(utf8_env)

    state.log.info("Making sure the divert-and-modify requirements are satisfied")
    state = pkg_apt.apt_install_if_needed(
        state, ["python3-pip", "python3-click", "python3-typedload"], allow_backports=False
    )
    if sys.version_info < (3, 11):
        state = pkg_apt.apt_install_if_needed(state, ["python3-tomli"], allow_backports=True)

    state.log.info("Installing divert-and-modify as a local package")
    state = install_divmod(state)
    state = find_divmod_bin(state)

    state.log.info("===== Starting the test steps")
    for idx, step in enumerate(steps.STEPS):
        state.log.info("===== Step %(idx)3d: %(desc)s", {"idx": idx + 1, "desc": str(step)})
        state = step.check(state)
    state.log.info("===== Done with all the steps!")


if __name__ == "__main__":
    main()
