# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Define and describe the steps that comprise the test."""

from __future__ import annotations

import dataclasses
import pathlib
import shlex
import stat
import subprocess
import sys
import typing

from . import pkg_apt


if typing.TYPE_CHECKING:
    from typing import Final

    from . import defs


@dataclasses.dataclass(frozen=True)
class Step:
    """A single test step."""

    def __str__(self) -> str:
        """Provide a human-readable description.

        Must be overridden by actual test step classes.
        """
        raise NotImplementedError(type(self).__str__)

    def check(self, _state: defs.State) -> defs.State:
        """Perform the actual check, raise an exception on failure.

        Must be overridden by actual test step classes.
        """
        raise NotImplementedError(type(self).check)


@dataclasses.dataclass(frozen=True)
class NoFileStep(Step):
    """Verify that a file does not exist."""

    path: pathlib.Path
    """The path to the file that must not exist."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"file does not exist: {self.path}"

    def check(self, state: defs.State) -> defs.State:
        """Make sure a file does, indeed, not exist."""
        if self.path.exists() or self.path.is_symlink():
            sys.exit(f"Did not expect {self.path} to exist")
        return state


@dataclasses.dataclass(frozen=True)
class FileStep(Step):
    """Verify that a file does not exist."""

    path: pathlib.Path
    """The path to the file that must exist."""

    uid: int | None
    """The owner user ID of the file."""

    gid: int | None
    """The group ID of the file."""

    mode: int | None
    """The file permissions mode (modulo 0o10000)."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return (
            f"file exists: {self.path} (uid {self.uid}, gid {self.gid}"
            + (f", mode {self.mode:04o}" if self.mode is not None else "")
            + ")"
        )

    def check(self, state: defs.State) -> defs.State:
        """Make sure a file exists and it has the expected attributes."""
        try:
            fstat: Final = self.path.lstat()
        except OSError as err:
            sys.exit(f"Could not examine {self.path}: {err}")

        bad: Final = []
        if not stat.S_ISREG(fstat.st_mode):
            bad.append(f"Expected {self.path} to be a regular file")
        if self.uid is not None and fstat.st_uid != self.uid:
            bad.append(f"Expected owner {self.uid}, got {fstat.st_uid}")
        if self.gid is not None and fstat.st_gid != self.gid:
            bad.append(f"Expected group {self.uid}, got {fstat.st_gid}")
        if self.mode is not None and (fstat.st_mode & 0o7777) != self.mode:
            bad.append(
                f"Expected permissions mode {self.mode:04o}, got {(fstat.st_mode & 0o7777):04o}"
            )

        if bad:
            sys.exit(f"File attributes mismatch for {self.path}: {'; '.join(bad)}")
        return state


@dataclasses.dataclass(frozen=True)
class DivListCountStep(Step):
    """There are exactly that many diversions attributed to the divert-and-modify package."""

    count: int
    """The number of diversions attributed to the divert-and-modify package."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"dpkg-divert --list reports {self.count} diversions for us"

    def check(self, state: defs.State) -> defs.State:
        """Count the suitable lines in the output of `dpkg-divert --list`."""
        lines: Final = subprocess.check_output(
            ["dpkg-divert", "--list"], encoding="UTF-8", env=state.utf8_env
        ).splitlines()
        count: Final = len([line for line in lines if line.endswith(" by divert-and-modify")])
        if count != self.count:
            sys.exit(
                f"Expected {self.count} divert-and-modify diversions, got {count} ones instead"
            )
        return state


@dataclasses.dataclass(frozen=True)
class DivModListContainsStep(Step):
    """The divert-and-modify tool knows about the specified step."""

    name: str
    """The name of the step to look for in the `list` output."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"divmod knows about a step: {self.name}"

    def check(self, state: defs.State) -> defs.State:
        """Look for a suitable line in the output of `divmod list`."""
        assert state.divmod_bin is not None  # noqa: S101  # mypy needs this
        lines: Final = subprocess.check_output(
            [state.divmod_bin, "list"], encoding="UTF-8", env=state.utf8_env
        ).splitlines()
        suffix: Final = "\t" + self.name
        if not any(line.endswith(suffix) for line in lines):
            sys.exit(f"The divmod list output does not contain a '{self.name}' step: {lines!r}")
        return state


@dataclasses.dataclass(frozen=True)
class DivModSync(Step):
    """Run `divert-and-modify sync`."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return "run divmod sync"

    def check(self, state: defs.State) -> defs.State:
        """Make sure `divmod sync` succeeds."""
        assert state.divmod_bin is not None  # noqa: S101  # mypy needs this
        subprocess.check_call([state.divmod_bin, "sync", "-v"], env=state.utf8_env)
        return state


@dataclasses.dataclass(frozen=True)
class DivModUndoAllStep(Step):
    """Run `divert-and-modify undo --all`."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return "run divmod undo --all"

    def check(self, state: defs.State) -> defs.State:
        """Make sure `divmod sync` succeeds."""
        assert state.divmod_bin is not None  # noqa: S101  # mypy needs this
        subprocess.check_call([state.divmod_bin, "undo", "--all", "-v"], env=state.utf8_env)
        return state


@dataclasses.dataclass(frozen=True)
class RunCommandStep(Step):
    """Run an arbitrary command."""

    cmd: list[str]
    """The command to run."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"run a command: {shlex.join(self.cmd)}"

    def check(self, state: defs.State) -> defs.State:
        """Make sure a command succeeds."""
        subprocess.check_call(self.cmd, env=state.utf8_env)
        return state


@dataclasses.dataclass(frozen=True)
class InstallPackageStep(Step):
    """Install an OS package."""

    name: str
    """The name of the package to install."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"install an OS package: {self.name}"

    def check(self, state: defs.State) -> defs.State:
        """Install a Debian package."""
        return pkg_apt.apt_install(state, [self.name])


@dataclasses.dataclass(frozen=True)
class RemovePackageStep(Step):
    """Remove an installed OS package."""

    name: str
    """The name of the package to remove."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"remove an OS package: {self.name}"

    def check(self, state: defs.State) -> defs.State:
        """Remove a Debian package."""
        return pkg_apt.apt_remove(state, [self.name])


@dataclasses.dataclass(frozen=True)
class FileHasLine(Step):
    """Make sure a line is present in a file."""

    path: pathlib.Path
    """The file to examine."""

    line: str
    """The exact full line to look for."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"the {self.path} file contains the '{self.line}' line"

    def check(self, state: defs.State) -> defs.State:
        """Examine the file, look for the line."""
        if self.line not in self.path.read_text(encoding="UTF-8").splitlines():
            sys.exit(f"The {self.path} file does not contain the '{self.line}' line")
        return state


@dataclasses.dataclass(frozen=True)
class FileLacksLine(Step):
    """Make sure a line is not present in a file."""

    path: pathlib.Path
    """The file to examine."""

    line: str
    """The exact full line to look for."""

    def __str__(self) -> str:
        """Provide a human-readable description."""
        return f"the {self.path} file does not contain the '{self.line}' line"

    def check(self, state: defs.State) -> defs.State:
        """Examine the file, look for the line."""
        if self.line in self.path.read_text(encoding="UTF-8").splitlines():
            sys.exit(f"The {self.path} file contains the '{self.line}' line")
        return state


PATH_GUTSY_ORIG: Final = pathlib.Path("/usr/share/debootstrap/scripts/gutsy")
PATH_GUTSY_DIVERTED: Final = PATH_GUTSY_ORIG.with_name(PATH_GUTSY_ORIG.name + ".divmod.v0")

LINE_GUTSY_ORIG: Final = "mirror_style release"
LINE_GUTSY_PATCHED: Final = "mirror_style main"

PATH_GPGZ_ORIG: Final = pathlib.Path("/usr/bin/gpg-zip")
PATH_GPGZ_DIVERTED: Final = PATH_GPGZ_ORIG.with_name(PATH_GPGZ_ORIG.name + ".divmod.v0")

LINE_GPGZ_ORIG: Final = "GPG=gpg"
LINE_GPGZ_PATCHED: Final = "GPG=gpg # and stuff"

PATH_DIVTEST_ORIG: Final = pathlib.Path("/usr/share/divtest")
PATH_DIVTEST_DIVERTED: Final = PATH_DIVTEST_ORIG.with_name(PATH_DIVTEST_ORIG.name + ".divmod.v0")

PATH_DIVTEST_SOURCE_ORIG: Final = pathlib.Path("/etc/hosts")
PATH_DIVTEST_SOURCE_PATCHED: Final = pathlib.Path("/etc/issue")

CMD_DIVTEST_DIVERT: Final = [
    "dpkg-divert",
    "--rename",
    "--divert",
    str(PATH_DIVTEST_DIVERTED),
    "--package",
    "divert-and-modify",
    "--add",
    str(PATH_DIVTEST_ORIG),
]


STEPS: Final = [
    DivListCountStep(0),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    NoFileStep(PATH_GUTSY_DIVERTED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    NoFileStep(PATH_GPGZ_DIVERTED),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModListContainsStep("debootstrap-release-mode"),
    DivModListContainsStep("gpg-zip-comment"),
    DivModSync(),
    DivListCountStep(2),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    NoFileStep(PATH_GUTSY_DIVERTED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RunCommandStep(["dpkg-divert", "--list"]),
    InstallPackageStep("debootstrap"),
    DivListCountStep(2),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    FileStep(PATH_GUTSY_DIVERTED, uid=0, gid=0, mode=0o644),
    FileHasLine(PATH_GUTSY_DIVERTED, LINE_GUTSY_ORIG),
    FileLacksLine(PATH_GUTSY_DIVERTED, LINE_GUTSY_PATCHED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    FileStep(PATH_GUTSY_ORIG, uid=0, gid=0, mode=0o644),
    FileStep(PATH_GUTSY_DIVERTED, uid=0, gid=0, mode=0o644),
    FileHasLine(PATH_GUTSY_ORIG, LINE_GUTSY_PATCHED),
    FileLacksLine(PATH_GUTSY_ORIG, LINE_GUTSY_ORIG),
    FileHasLine(PATH_GUTSY_DIVERTED, LINE_GUTSY_ORIG),
    FileLacksLine(PATH_GUTSY_DIVERTED, LINE_GUTSY_PATCHED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RemovePackageStep("debootstrap"),
    DivListCountStep(2),
    #
    FileStep(PATH_GUTSY_ORIG, uid=0, gid=0, mode=0o644),
    NoFileStep(PATH_GUTSY_DIVERTED),
    FileHasLine(PATH_GUTSY_ORIG, LINE_GUTSY_PATCHED),
    FileLacksLine(PATH_GUTSY_ORIG, LINE_GUTSY_ORIG),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    NoFileStep(PATH_GUTSY_DIVERTED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RunCommandStep(CMD_DIVTEST_DIVERT),
    DivListCountStep(3),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RunCommandStep(
        [
            "install",
            "-o",
            "root",
            "-g",
            "root",
            "-m",
            "644",
            "--",
            str(PATH_DIVTEST_SOURCE_ORIG),
            str(PATH_DIVTEST_ORIG),
        ]
    ),
    DivListCountStep(2),
    #
    FileStep(PATH_DIVTEST_ORIG, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_ORIG)]),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    FileStep(PATH_DIVTEST_ORIG, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_ORIG)]),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RunCommandStep(CMD_DIVTEST_DIVERT),
    DivListCountStep(3),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    FileStep(PATH_DIVTEST_DIVERTED, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_DIVERTED)]),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    FileStep(PATH_DIVTEST_ORIG, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_ORIG)]),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    RunCommandStep(CMD_DIVTEST_DIVERT),
    DivListCountStep(3),
    #
    NoFileStep(PATH_DIVTEST_ORIG),
    FileStep(PATH_DIVTEST_DIVERTED, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_DIVERTED)]),
    #
    RunCommandStep(
        [
            "install",
            "-o",
            "root",
            "-g",
            "root",
            "-m",
            "644",
            "--",
            str(PATH_DIVTEST_SOURCE_PATCHED),
            str(PATH_DIVTEST_ORIG),
        ]
    ),
    #
    DivListCountStep(3),
    FileStep(PATH_DIVTEST_ORIG, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_PATCHED), str(PATH_DIVTEST_ORIG)]),
    FileStep(PATH_DIVTEST_DIVERTED, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_DIVERTED)]),
    #
    DivModSync(),
    DivListCountStep(2),
    #
    FileStep(PATH_DIVTEST_ORIG, uid=0, gid=0, mode=0o644),
    RunCommandStep(["cmp", "--", str(PATH_DIVTEST_SOURCE_ORIG), str(PATH_DIVTEST_ORIG)]),
    NoFileStep(PATH_DIVTEST_DIVERTED),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    NoFileStep(PATH_GUTSY_DIVERTED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    FileStep(PATH_GPGZ_DIVERTED, uid=0, gid=0, mode=0o755),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileHasLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_DIVERTED, LINE_GPGZ_PATCHED),
    #
    DivModUndoAllStep(),
    DivListCountStep(0),
    #
    NoFileStep(PATH_GUTSY_ORIG),
    NoFileStep(PATH_GUTSY_DIVERTED),
    #
    FileStep(PATH_GPGZ_ORIG, uid=0, gid=0, mode=0o755),
    NoFileStep(PATH_GPGZ_DIVERTED),
    FileHasLine(PATH_GPGZ_ORIG, LINE_GPGZ_ORIG),
    FileLacksLine(PATH_GPGZ_ORIG, LINE_GPGZ_PATCHED),
]
