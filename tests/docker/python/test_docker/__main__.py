# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Command-line tool for applying file diversions and updates."""

from __future__ import annotations

import dataclasses
import pathlib
import subprocess
import sys
import typing

import click

from divert_and_modify import util as divmod_util


if typing.TYPE_CHECKING:
    import logging
    from typing import Final


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the Docker test tool."""

    log: logging.Logger
    """Send diagnostic messages and stuff."""

    utf8_env: dict[str, str]
    """UTF-8-capable environment settings for running child processes."""


def build_wheel(cfg: Config, cwd: pathlib.Path) -> pathlib.Path:
    """Package up the divert-and-modify library."""
    distdir: Final = cwd / "dist"
    if distdir.exists() or distdir.is_symlink():
        cfg.log.debug("Removing %(distdir)s", {"distdir": distdir})
        subprocess.check_call(["rm", "-rf", "--", distdir], env=cfg.utf8_env)

    cfg.log.debug("Building the divert-and-modify Python wheel")
    subprocess.check_call([sys.executable, "-m", "build", "--wheel"], env=cfg.utf8_env)

    files: Final = [path for path in distdir.iterdir() if path.is_file()]
    if len(files) != 1:
        sys.exit(f"Expected a single file in {distdir} after `python3 -m build`, got {files}")
    wheel: Final = files[0]
    if not wheel.name.endswith(".whl"):
        sys.exit(f"Expected a *.whl file in {distdir} after `python3 -m build`, got {wheel}")
    return wheel


@click.command(name="run")
@click.option("--image", type=str, default="divmod:bullseye")
def _cmd_run(*, image: str) -> None:
    """Run a test within a Docker container."""
    cfg: Final = Config(
        log=divmod_util.build_logger(verbose=True),
        utf8_env=divmod_util.get_deb_utf8_environment(),
    )
    cwd: Final = pathlib.Path.cwd()
    wheel: Final = build_wheel(cfg, cwd)
    docker_dir: Final = cwd / "tests/docker"
    try:
        subprocess.check_call(
            [
                "docker",
                "run",
                "--rm",
                "-v",
                f"{wheel.parent}:/opt/dist:ro",
                "-v",
                f"{docker_dir / 'python'}:/opt/test-python:ro",
                "-v",
                f"{docker_dir / 'share/divert-and-modify'}:/usr/share/divert-and-modify:ro",
                "-v",
                f"{docker_dir / 'etc/divert-and-modify'}:/etc/divert-and-modify:ro",
                "--",
                image,
                "env",
                "PYTHONPATH=/opt/test-python",
                "python3",
                "-B",
                "-u",
                "-m",
                "test_functional",
            ],
            env=cfg.utf8_env,
        )
    except (OSError, subprocess.CalledProcessError) as err:
        sys.exit(f"The Docker test failed: {err}")


@click.group(name="test-docker")
def main() -> None:
    """Run subcommands."""


main.add_command(_cmd_run)


if __name__ == "__main__":
    main()
