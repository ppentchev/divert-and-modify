# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test the operation of the divert_and_modify.apply routines."""

from __future__ import annotations

import dataclasses
import functools
import logging
import pathlib
import subprocess
import sys
import tempfile
import typing
from unittest import mock

import pytest

import divert_and_modify as div_mod
from divert_and_modify import apply as div_apply
from divert_and_modify import config as div_config


if typing.TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Final, List, Union  # noqa: UP035

    PathUnion = Union[pathlib.Path, str]
    PathList = List[PathUnion]  # noqa: UP006


@functools.lru_cache
def get_utf8_env() -> dict[str, str]:
    """Prepare a UTF-8-capable environment."""
    return div_mod.get_deb_utf8_environment()


@functools.lru_cache
def build_logger() -> logging.Logger:
    """Build a logger for test purposes."""
    log: Final = logging.getLogger("unit_tests.divert_and_modify.test_apply")
    log.propagate = False
    log.setLevel(logging.DEBUG)

    handler: Final = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    log.addHandler(handler)
    return log


def test_apply_to_file_fail() -> None:
    """Test various scenarios where apply_to_file() may fail."""

    def modify_not_invoked(cfg: div_mod.Config, dfile: div_mod.DivertChange, contents: str) -> str:
        """Complain loudly about our calm being disturbed."""
        raise AssertionError(repr((cfg, dfile, contents)))

    with tempfile.TemporaryDirectory() as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        tempf: Final = tempd / "some.file"

        cfg: Final = div_mod.Config(log=build_logger(), utf8_env=get_utf8_env())
        dfile: Final = div_mod.DivertChange(name="no-change", path=tempf, modify=modify_not_invoked)

        def do_mock_dpkg_divert_list(
            output: Exception | str, cmd: PathList, *, encoding: str, env: dict[str, str]
        ) -> str:
            """Raise an error."""
            assert (cmd, encoding, env) == (
                ["dpkg-divert", "--list", "--", tempf],
                "UTF-8",
                cfg.utf8_env,
            )
            if isinstance(output, Exception):
                raise output
            return output

        def mock_dpkg_divert_list(output: Exception | str) -> Callable[..., str]:
            """Shorthand for invoking `do_mock_dpkg_divert_list`."""
            return lambda *args, **kwargs: do_mock_dpkg_divert_list(output, *args, **kwargs)

        with pytest.raises(RuntimeError):
            div_apply.apply_to_file(
                cfg, dfile.path, [dfile, dataclasses.replace(dfile, path=tempd)]
            )

        with mock.patch(
            "subprocess.check_output",
            new=mock_dpkg_divert_list(subprocess.CalledProcessError(42, [])),
        ), pytest.raises(div_apply.ExamineDivertError):
            div_apply.apply_to_file(cfg, dfile.path, [dfile])


def test_load_and_apply() -> None:
    """Load the sample file."""
    cwd: Final = pathlib.Path.cwd()
    docker_dir: Final = cwd / "tests/docker"
    etc_dir: Final = docker_dir / "etc/divert-and-modify/changes.d"
    share_dir: Final = docker_dir / "share/divert-and-modify/changes.d"

    cfg: Final = div_mod.Config(log=build_logger(), utf8_env=get_utf8_env())
    changes: Final = div_config.load_changes(cfg, dirs=[share_dir, etc_dir])
    assert [
        (change.name, change.path, change.same_ok, change.allowed_suffixes) for change in changes
    ] == [
        (
            "debootstrap-release-mode",
            pathlib.Path("/usr/share/debootstrap/scripts/gutsy"),
            False,
            [],
        ),
        ("gpg-zip-comment", pathlib.Path("/usr/bin/gpg-zip"), True, [".zip-comment"]),
    ]
